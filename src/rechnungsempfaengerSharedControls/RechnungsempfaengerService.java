package rechnungsempfaengerSharedControls;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import datenbankService.DatenbankException;
import datenbankService.DatenbankService;

public class RechnungsempfaengerService  extends DatenbankService<RechnungsempfaengerImpl>{

	public RechnungsempfaengerService() throws DatenbankException {
		super();
	}

	@Override
	protected RechnungsempfaengerImpl fillImpl(ResultSet resultSet) {
		RechnungsempfaengerImpl impl = new RechnungsempfaengerImpl();
		try {
			impl.setHausnumer(resultSet.getString("hausnummer"));
			impl.setName(resultSet.getString("name"));
			impl.setOrt(resultSet.getString("ort"));
			impl.setPlz(resultSet.getString("plz"));
			impl.setStrasse(resultSet.getString("strasse"));
			impl.setSurrkey_tour(resultSet.getInt("surrkey_kunde"));
			impl.setSurrkey(resultSet.getInt("SURRKEY_RECHNUNGSEMPFAENGER"));
			
		}catch(SQLException e) {
			
		}
		return impl;
	}
	
	
	public boolean saveOne(RechnungsempfaengerImpl impl) {
		String sql  = createInsertOrUpdateStatment(impl);
		boolean saved = excecute(sql);
		return saved;
	}
	
	
	public List<RechnungsempfaengerImpl> findAll (String... wheres) throws DatenbankException {
		List<RechnungsempfaengerImpl> listImpl = new ArrayList<RechnungsempfaengerImpl>();
		String where = generateWhereStatment(wheres);
		String sql = "select * from r_rechnungsempfaenger";
		sql += where.equals("")?"": " where " +  where;
		ResultSet resultSet = find(sql);
		if(resultSet != null) {
			try {
				while(resultSet.next()) {
					listImpl.add(fillImpl(resultSet));
				}
			} catch (SQLException e) {
				throw new DatenbankException(e);
			}
		}
		return listImpl;
	}
	
	public int getNextSurrkey() throws SQLException {
		return generateNewSurrkey("r_rechnungsempfaenger", "surrkey_rechnungsempfaenger");
	}
	
	public int findSurrkey(String... wheres) throws DatenbankException {
		String where = generateWhereStatment(wheres);
		String sql = "select surrkey_rechnungsempfaenger from r_rechnungsempfaenger";
		sql += where.equals("")?"": " where " + where;
		ResultSet resultSet = find(sql);
		if(resultSet != null) {
			try {
				while(resultSet.next()) {
					return resultSet.getInt("surrkey_rechnungsempfaenger");
				}
				
			}catch(SQLException e) {
				throw new DatenbankException(e);
			}
		}else {
			
		}
		return -1;
	}
	
	public RechnungsempfaengerImpl findOne(int surrkey) throws DatenbankException {
		String sql = "select * from r_rechnungsempfaenger where surrkey_rechnungsempfaenger = " + surrkey;
		ResultSet resultSet = find(sql);
		if(resultSet != null) {
			try {
				while(resultSet.next()) {
					return fillImpl(resultSet);
				}
			} catch (SQLException e) {
				throw new DatenbankException(e);
			}
		}
		return null;
		
	}

}
