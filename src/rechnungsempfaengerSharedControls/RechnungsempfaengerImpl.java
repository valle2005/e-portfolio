package rechnungsempfaengerSharedControls;

import java.lang.reflect.Field;
import java.util.HashMap;

import sharedControls.AbstractImpl;
import sharedControls.UnvalideDataException;

public class RechnungsempfaengerImpl extends AbstractImpl<RechnungsempfaengerImpl>{

	
	private String name ="";
	private String strasse ="";
	private String hausnummer="";
	private String plz ="";
	private String ort = "";
	private int surrkey_kunde;
	
	public RechnungsempfaengerImpl() {
		super("r_rechnungsempfaenger", "surrkey_rechnungsempfaenger");
	}

	@Override
	public HashMap<String, Object> generateDatabaseHashSet() {
		HashMap< String, Object> data = new HashMap<String, Object>();
		data.put("surrkey_rechnungsempfaenger", getSurrkey());
		for(Field field:getClass().getDeclaredFields()) {
			try {
				data.put(field.getName(), field.get(this));
			} catch (IllegalArgumentException | IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return data;
	}

	@Override
	public boolean isEqual(RechnungsempfaengerImpl abstractImpl) {
		if(!name.equals(abstractImpl.getName()))return false;
		if(!strasse.equals(abstractImpl.getStrasse())) return false;
		if(!hausnummer.equals(abstractImpl.getHausnumer())) return false;
		if(!plz.equals(abstractImpl.getPlz()))	return false;
		if(!ort.equals(abstractImpl.getOrt())) return false;
		if(surrkey_kunde != abstractImpl.getSurrkey_tour()) return false;
		return true;
	}

	
	public String toString() {
		return name + "|" + ort;
	}
	
	
	@Override
	public boolean ueberpruefeDaten() throws UnvalideDataException {
		return true;
	}

	@Override
	public Object[] createSuchergebnis() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStrasse() {
		return strasse;
	}

	public void setStrasse(String strasse) {
		this.strasse = strasse;
	}

	public String getHausnumer() {
		return hausnummer;
	}

	public void setHausnumer(String hausnumer) {
		this.hausnummer = hausnumer;
	}

	public String getPlz() {
		return plz;
	}

	public void setPlz(String plz) {
		this.plz = plz;
	}

	public String getOrt() {
		return ort;
	}

	public void setOrt(String ort) {
		this.ort = ort;
	}

	public int getSurrkey_tour() {
		return surrkey_kunde;
	}

	public void setSurrkey_tour(int surrkey_tour) {
		this.surrkey_kunde = surrkey_tour;
	}

	
	
}
