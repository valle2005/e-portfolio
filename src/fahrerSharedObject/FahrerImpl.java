package fahrerSharedObject;

import java.lang.reflect.Field;
import java.util.HashMap;

import sharedControls.AbstractImpl;
import sharedControls.UnvalideDataException;

public class FahrerImpl extends AbstractImpl<FahrerImpl>{

	
	private int fahrer_nr = 0;
	private String name ="";
	private String vorname ="";
	private int preisfaktor = 0;
	
	public FahrerImpl() {
		super("s_fahrer", "surrkey_fahrer");
	}

	@Override
	public HashMap<String, Object> generateDatabaseHashSet() {
		HashMap< String, Object> data = new HashMap<String, Object>();
		data.put("surrkey_fahrer", getSurrkey());
		for(Field field:getClass().getDeclaredFields()) {
			try {
				data.put(field.getName(), field.get(this));
			} catch (IllegalArgumentException | IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		return data;
	}
	
	public String toString() {
		return name + " " + vorname;
	}

	@Override
	public boolean isEqual(FahrerImpl abstractImpl) {
		if(fahrer_nr != abstractImpl.getFahrer_nr() ) return false;
		if(!name.equals(abstractImpl.getName())) return false;
		if(!vorname.equals(abstractImpl.getVorname())) return false;
		if(preisfaktor != abstractImpl.getPreisfaktor()) return false;
		return true;
	}

	@Override
	public boolean ueberpruefeDaten() throws UnvalideDataException {
		if(name.equals("")) throw new UnvalideDataException("Name", "Der Name darf nicht leer sein");
		if(preisfaktor > 100) throw new UnvalideDataException("Prozente", "D�rfen nicht gr��er sein als 100");
		if(preisfaktor < 0) throw new UnvalideDataException("Prozente", "M�ssen gr��er als 0 sein");
		return true;
	}

	@Override
	public Object[] createSuchergebnis() {
		return new Object[]{getFahrer_nr(), getName(), getVorname(), getPreisfaktor()};
	}

	public int getFahrer_nr() {
		return fahrer_nr;
	}

	public void setFahrer_nr(int fahrer_nr) {
		this.fahrer_nr = fahrer_nr;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	public int getPreisfaktor() {
		return preisfaktor;
	}

	public void setPreisfaktor(int preisfaktor) {
		this.preisfaktor = preisfaktor;
	}

}
