package fahrerSharedObject;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import datenbankService.DatenbankException;
import datenbankService.DatenbankService;

public class FahrerService extends DatenbankService<FahrerImpl> {

	public FahrerService() throws DatenbankException {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	protected FahrerImpl fillImpl(ResultSet resultSet) {
		FahrerImpl impl = new FahrerImpl();
		try {
			impl.setSurrkey(resultSet.getInt("surrkey_fahrer"));
			impl.setFahrer_nr(resultSet.getInt("fahrer_nr"));
			impl.setName(resultSet.getString("name"));
			impl.setVorname(resultSet.getString("vorname"));
			impl.setPreisfaktor(resultSet.getInt("preisfaktor"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return impl;
	}
	
	
	public boolean saveFahrerImpl(FahrerImpl impl) {
		String sql = createInsertOrUpdateStatment(impl);
		boolean saved = excecute(sql);
		return saved;
	}

	
	public FahrerImpl findOne(int surrkey) throws DatenbankException {
		String sql = "select * from s_fahrer where surrkey_fahrer = " + surrkey;
		ResultSet resultSet = find(sql);
		FahrerImpl impl =null;
		try {
			while(resultSet.next()) {
				impl = fillImpl(resultSet);
			}
		} catch (SQLException e) {
			throw new DatenbankException(e);
		}
		return impl;
	}
	
	public FahrerImpl findOneByNr(int fahrer_nr) throws DatenbankException {
		String sql = "select * from S_fahrer where fahrer_nr = "+ fahrer_nr;
		ResultSet resultSet = find(sql);
		FahrerImpl impl = null;
		try {
			if(resultSet != null) {
				while(resultSet.next()) {
					impl = fillImpl(resultSet);
				}
			}
		}catch(SQLException e) {
				throw new DatenbankException(e);
		}
		
		return impl;
	}
	
	public List<FahrerImpl> findAll(String...wheres) throws DatenbankException {
		String sql = "select * from s_fahrer";
		String where = generateWhereStatment(wheres);
		sql += where.equals("")?"": " where " + where;
		sql += " order by fahrer_nr";
		List<FahrerImpl> implList = new ArrayList<FahrerImpl>();
		ResultSet resultSet = find(sql);
		if(resultSet != null) {
		try {
			while(resultSet.next()) {
					implList.add(fillImpl(resultSet));
				}
			} catch (SQLException e) {
				throw new DatenbankException(e);
			}
		}
		return implList;
	}
}
