package tourSharedControls;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import datenbankService.DatenbankException;
import datenbankService.DatenbankService;
import empfaengerSharedControls.EmpfaengerImpl;

public class TourService  extends DatenbankService<TourImpl>{

	public TourService() throws DatenbankException {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	protected TourImpl fillImpl(ResultSet resultSet) {
		TourImpl impl = new TourImpl();
		try {
			impl.setSurrkey(resultSet.getInt("surrkey_tour"));
			impl.setAbsender(resultSet.getString("absender"));
			impl.setAbsender_hausnummer(resultSet.getString("absender_hausnummer"));
			impl.setAbsender_ort(resultSet.getString("absender_ort"));
			impl.setAbsender_plz(resultSet.getString("absender_plz"));
			impl.setAbsender_strasse(resultSet.getString("absender_strasse"));
			impl.setAbsender_tel(resultSet.getString("absender_tel"));
			impl.setKilometer(resultSet.getDouble("kilometer"));
			impl.setKunden_nr(resultSet.getInt("kunden_nr"));
			impl.setSurrkey_kunde(resultSet.getInt("surrkey_kunde"));
			impl.setTour_id(resultSet.getInt("tour_id"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return impl;
	}

	
	public TourImpl findOne(int surrkey) throws DatenbankException {
		String sql = "select * from s_tour where surrkey_tour = " + surrkey;
		ResultSet resultSet = find(sql);
		TourImpl impl = new TourImpl();
		try {
			while(resultSet.next()) {
				impl = fillImpl(resultSet);
			}
		} catch (SQLException e) {
			throw new DatenbankException(e);
		}
		
		return impl;
	}
	
	public int getCount() throws DatenbankException {
		String sql = "select count(*) from s_tour";
		ResultSet resultSet = find(sql);
		int count = 0;
		try {
			while (resultSet.next()) {
				count = resultSet.getInt("count(*)");
			}
		} catch (SQLException e) {
			throw new DatenbankException(e);
		}
		return count;
	}
	
	public boolean saveOne(TourImpl impl) {
		String sql = createInsertOrUpdateStatment(impl);
		boolean saved = excecute(sql);
		return saved;
	}
	
	public int nextSurrkey() throws DatenbankException {
		try {
			return generateNewSurrkey("s_tour", "surrkey_tour");
		} catch (SQLException e) {
			throw new DatenbankException(e);
		}
	}
	
	
	public List<TourImpl> findAll(String...wheres) throws DatenbankException {
		String sql = "select distinct s.* from s_tour s, s_empfaenger b";
		String where = generateWhereStatment(wheres);
		sql += !where.equals("")?" where " + where:"";
		List<TourImpl> liste = new ArrayList<TourImpl>();
		ResultSet resultSet = find(sql);
		try {
			if(resultSet != null) {
				while (resultSet.next()) {
					liste.add(fillImpl(resultSet));
				}
			}
		} catch (SQLException e) {
			throw new DatenbankException(e);
		}
		
		return liste;
	}
	
}
