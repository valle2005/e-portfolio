package tourSharedControls;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import datenbankService.DatenbankException;
import empfaengerSharedControls.EmpfaengerImpl;
import empfaengerSharedControls.EmpfaengerService;
import sharedControls.AbstractImpl;
import sharedControls.UnvalideDataException;

public class TourImpl extends AbstractImpl<TourImpl>{

	
	private String absender ="";
	private String absender_hausnummer ="";
	private String absender_ort ="";
	private String absender_plz ="";
	private String absender_strasse ="";
	private String absender_tel ="";
	private double kilometer ;
	private int kunden_nr = 0;
	private int surrkey_kunde;
	private int tour_id;
	
	
	
	
	@Override
	public HashMap<String, Object> generateDatabaseHashSet() {
		HashMap< String, Object> data = new HashMap<String, Object>();
		data.put("surrkey_tour", getSurrkey());
		for(Field field:getClass().getDeclaredFields()) {
			try {
				data.put(field.getName(), field.get(this));
			} catch (IllegalArgumentException | IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		return data;

	}

	@Override
	public boolean isEqual(TourImpl abstractImpl) {
		if(!absender.equals(abstractImpl.getAbsender())) return false;
		if(!absender_hausnummer.equals(abstractImpl.getAbsender_hausnummer())) return false;
		if(!absender_ort.equals(abstractImpl.getAbsender_ort())) return false;
		if(!absender_plz.equals(abstractImpl.getAbsender_plz())) return false;
		if(!absender_strasse.equals(abstractImpl.getAbsender_strasse()))return false;
		if(!absender_tel.equals(abstractImpl.getAbsender_tel())) return false;
		if(kilometer != abstractImpl.getKilometer()) return false;
		if(kunden_nr != abstractImpl.getKunden_nr()) return false;
		if(surrkey_kunde != abstractImpl.getSurrkey_kunde()) return false;
		return true;
	}

	@Override
	public boolean ueberpruefeDaten() throws UnvalideDataException {
		if(absender.equals("")) throw new UnvalideDataException("Absender", "Der Absender darf nicht leer sein");
		if(kilometer <= 0) throw new UnvalideDataException("Kilometer", "Die Kilometer m�ssen gr��er 0 sein");
		if(absender_ort.equals("")) throw new UnvalideDataException("Absender Ort", "Der Absender Ort darf nicht leer sein");
		return true;
	}

	@Override
	public Object[] createSuchergebnis() {
		EmpfaengerService empfaengerService = null;
		try {
		empfaengerService = new EmpfaengerService();
		} catch (DatenbankException e) {
		e.printStackTrace();
		}
		List<EmpfaengerImpl> empfaengerList = new ArrayList<EmpfaengerImpl>();
		int i = 0;
		try {
			empfaengerList = empfaengerService.findAll("surrkey_tour = " + getSurrkey());
		} catch (DatenbankException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String empfaenger = "";
		for(EmpfaengerImpl impl: empfaengerList) {
			empfaenger += "/" + impl.getEmpfaenger();
		}
		
		return new Object[]{kunden_nr, absender, empfaenger, kilometer};
	}
	
	public List<EmpfaengerImpl> getEmpfaegner() {
		EmpfaengerService service = null;
		try {
			service = new EmpfaengerService();
		}catch(DatenbankException e) {
			
		}
		List<EmpfaengerImpl> implList = new ArrayList<EmpfaengerImpl>();
		try {
			implList = service.findAll(" surrkey_tour = "+ getSurrkey());
		}catch(DatenbankException e) {
			e.printStackTrace();
		}
		return implList;
	}

	public String toString() {
		EmpfaengerService service = null;
		try {
			service = new EmpfaengerService();
		} catch (DatenbankException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		List<EmpfaengerImpl> implList = new ArrayList<EmpfaengerImpl>();
		String empfaenger ="";
		try {
			implList = service.findAll(" surrkey_tour = " + getSurrkey());
			for(EmpfaengerImpl impl: implList) {
				empfaenger += "/" + impl.getEmpfaenger();
			}
		} catch (DatenbankException e) {
			e.printStackTrace();
		}
		return absender  + " \u2192 " + empfaenger.substring(1); 
	}
	
	public synchronized String getAbsender() {
		return absender;
	}

	public synchronized void setAbsender(String absender) {
		this.absender = absender;
	}

	public synchronized String getAbsender_hausnummer() {
		return absender_hausnummer;
	}

	public synchronized void setAbsender_hausnummer(String absender_hausnummer) {
		this.absender_hausnummer = absender_hausnummer;
	}

	public synchronized String getAbsender_ort() {
		return absender_ort;
	}

	public synchronized void setAbsender_ort(String absender_ort) {
		this.absender_ort = absender_ort;
	}

	public synchronized String getAbsender_plz() {
		return absender_plz;
	}

	public synchronized void setAbsender_plz(String absender_plz) {
		this.absender_plz = absender_plz;
	}

	public synchronized String getAbsender_strasse() {
		return absender_strasse;
	}

	public synchronized void setAbsender_strasse(String absender_strasse) {
		this.absender_strasse = absender_strasse;
	}

	public synchronized String getAbsender_tel() {
		return absender_tel;
	}

	public synchronized void setAbsender_tel(String absender_tel) {
		this.absender_tel = absender_tel;
	}

	public synchronized double getKilometer() {
		return kilometer;
	}

	public synchronized void setKilometer(double kilometer) {
		this.kilometer = kilometer;
	}

	public synchronized int getKunden_nr() {
		return kunden_nr;
	}

	public synchronized void setKunden_nr(int kunden_nr) {
		this.kunden_nr = kunden_nr;
	}

	public synchronized int getSurrkey_kunde() {
		return surrkey_kunde;
	}

	public synchronized void setSurrkey_kunde(int surrkey_kunde) {
		this.surrkey_kunde = surrkey_kunde;
	}

	public synchronized int getTour_id() {
		return tour_id;
	}

	public synchronized void setTour_id(int tour_id) {
		this.tour_id = tour_id;
	}

	public TourImpl() {
		super("s_tour", "surrkey_tour");
	}

	
	
	
}
