package preiseSharedControls;

import java.util.ArrayList;
import java.util.List;
import java.sql.ResultSet;
import java.sql.SQLException;

import datenbankService.DatenbankException;
import datenbankService.DatenbankService;

public class PreisService extends DatenbankService<PreisImpl> {

	public PreisService() throws DatenbankException {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	protected PreisImpl fillImpl(ResultSet resultSet) {
		PreisImpl impl = new PreisImpl();
		try {
			if(resultSet.getInt("surrkey_preis") != 0) {
				impl.setSurrkey(resultSet.getInt("surrkey_preis"));
			}
			if(resultSet.getDouble("anfahrtspauschale") != 0) {
				impl.setAnfahrtspauschale(resultSet.getDouble("anfahrtspauschale"));
			}
			if(resultSet.getString("fahrzeug") != null) {
				impl.setFahrzeug(resultSet.getString("fahrzeug"));
			}
			if(resultSet.getDouble("ladezeit") != 0) {
				impl.setLadezeit(resultSet.getDouble("ladezeit"));
			}
			if(resultSet.getDouble("min_preis") != 0) {
				impl.setMin_preis(resultSet.getDouble("min_preis"));;
			}
			if(resultSet.getInt("preis_id") != 0) {
				impl.setPreis_id(resultSet.getInt("preis_id"));
			}
			if(resultSet.getDouble("pro_km") != 0) {
				impl.setPro_km(resultSet.getDouble("pro_km"));
			}
			if(resultSet.getDouble("rueckfracht") != 0) {
				impl.setRueckfracht(resultSet.getDouble("rueckfracht"));
			}
			if(resultSet.getDouble("stopps") != 0) {
				impl.setStopps(resultSet.getDouble("stopps"));
			}
			impl.setSurrkey_zusatzleistung(resultSet.getInt("SURRKEY_ZUSATZLEISTUNG"));
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return impl;
	}
	
	
	public int getZusatzInfoByID(int id) throws DatenbankException {
		String sql = "select distinct surrkey_zusatzleistung from s_preis where preis_id = " + id;
		ResultSet resultSet = find(sql);
		int surrkey = -1;
		if(resultSet != null) {
			try {
				while(resultSet.next()) {
					surrkey = resultSet.getInt("surrkey_zusatzleistung");
				}
			} catch (SQLException e) {
				throw new DatenbankException(e);
			}
		}
		return surrkey;
		
	}
	
	
	public boolean savePreisImpl(PreisImpl impl) {
		String sql = createInsertOrUpdateStatment(impl);
		boolean saved = excecute(sql);
		return saved;
	}
	
	public PreisImpl findOne(int surrkey)  throws DatenbankException{
		PreisImpl impl = new PreisImpl();
		String sql = "select * from s_preis where surrkey_preis = " + surrkey;
		ResultSet resultSet = find(sql);
		try {
			while(resultSet.next()) {
				impl = fillImpl(resultSet);
			}
			return impl;
		} catch (SQLException e) {
			throw new DatenbankException(e);
		}
	}
	
	
	public List<PreisImpl>  findAll(String... wheres) throws DatenbankException {
		List<PreisImpl> implList = new ArrayList<PreisImpl>();
		String sql = "select * from s_preis";
		String whereStatment = generateWhereStatment(wheres);
		sql += whereStatment.equals("")?"": " where " + whereStatment;
		ResultSet resultSet = find(sql);
		try {
			if(resultSet != null) 
				while(resultSet.next()) {
					{
						implList.add(fillImpl(resultSet));
					}
			}
		} catch (SQLException e) {
			throw new DatenbankException(e);
		}
		return implList;
		
	}
	
	public List<String> findAllId() throws DatenbankException {
		List<String> ids = new ArrayList<String>();
		String sql = "select distinct preis_id from s_preis";
		ResultSet resultSet = find(sql);
		try {
			while(resultSet.next()) {
				ids.add("" + resultSet.getInt("preis_id"));
			}
		} catch (SQLException e) {
			throw new DatenbankException(e);
		}
		return ids;
	}

}
