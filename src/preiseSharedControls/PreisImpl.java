package preiseSharedControls;

import java.lang.reflect.Field;
import java.util.HashMap;

import sharedControls.AbstractImpl;
import sharedControls.UnvalideDataException;

public class PreisImpl extends AbstractImpl<PreisImpl>{

	
	private double anfahrtspauschale = 0.00;
	private String fahrzeug = "";
	private double ladezeit = 0.00;
	private double min_preis = 0.00;
	private int preis_id = 0;
	private double pro_km = 0.00;
	private double rueckfracht = 0.00;
	private double stopps = 0.00;
	private int surrkey_zusatzleistung = -1;
	
	
	
	public int getSurrkey_zusatzleistung() {
		return surrkey_zusatzleistung;
	}

	public void setSurrkey_zusatzleistung(int surrkey_zusatzleistung) {
		this.surrkey_zusatzleistung = surrkey_zusatzleistung;
	}

	public PreisImpl() {
		super("S_PREIS", "surrkey_preis");
		// TODO Auto-generated constructor stub
	}

	
	public String toString() {
		return fahrzeug;
	}
	
	
	@Override
	public HashMap<String, Object> generateDatabaseHashSet() {
		HashMap< String, Object> data = new HashMap<String, Object>();
		data.put("surrkey_preis", getSurrkey());
		for(Field field:getClass().getDeclaredFields()) {
			try {
				data.put(field.getName(), field.get(this));
			} catch (IllegalArgumentException | IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return data;
	}

	@Override
	public boolean isEqual(PreisImpl abstractImpl) {
		if(anfahrtspauschale != abstractImpl.getAnfahrtspauschale()) return false;
		if(!fahrzeug.equals(abstractImpl.getFahrzeug())) return false;
		if(ladezeit != abstractImpl.getLadezeit()) return false;
		if(min_preis != abstractImpl.getMin_preis()) return false;
		if(preis_id != abstractImpl.getPreis_id()) return false;
		if(pro_km != abstractImpl.getPro_km()) return false;
		if(rueckfracht != abstractImpl.getRueckfracht()) return false;
		if(stopps != abstractImpl.getStopps()) return false;
		//TODO zusatzleistunden
		return true;
	}

	@Override
	public boolean ueberpruefeDaten() throws UnvalideDataException {
		if(fahrzeug.equals("")) throw new UnvalideDataException("Fahrzeug", "Fahrzeug darf nicht leer sein!");
		if(preis_id <= 100 ) throw new UnvalideDataException("Preis-ID", "Preis ID muss gr��er sein als 100");
		return false;
	}

	@Override
	public Object[] createSuchergebnis() {
		return new Object[]{getFahrzeug(), getPro_km(), getStopps(), getAnfahrtspauschale()};
	}

	public double getAnfahrtspauschale() {
		return anfahrtspauschale;
	}

	public void setAnfahrtspauschale(double anfahrtspauschale) {
		this.anfahrtspauschale = anfahrtspauschale;
	}

	public String getFahrzeug() {
		return fahrzeug;
	}

	public void setFahrzeug(String fahrzeug) {
		this.fahrzeug = fahrzeug;
	}

	public double getLadezeit() {
		return ladezeit;
	}

	public void setLadezeit(double ladezeit) {
		this.ladezeit = ladezeit;
	}

	public double getMin_preis() {
		return min_preis;
	}

	public void setMin_preis(double min_preis) {
		this.min_preis = min_preis;
	}

	public int getPreis_id() {
		return preis_id;
	}

	public void setPreis_id(int preis_id) {
		this.preis_id = preis_id;
	}

	public double getPro_km() {
		return pro_km;
	}

	public void setPro_km(double pro_km) {
		this.pro_km = pro_km;
	}

	public double getRueckfracht() {
		return rueckfracht;
	}

	public void setRueckfracht(double rueckfracht) {
		this.rueckfracht = rueckfracht;
	}

	public double getStopps() {
		return stopps;
	}

	public void setStopps(double stopps) {
		this.stopps = stopps;
	}
	

}
