package fahrtSharedControls;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import datenbankService.DatenbankException;
import datenbankService.DatenbankService;

public class FahrtService  extends DatenbankService<FahrtImpl>{

	public FahrtService() throws DatenbankException {
		super();
	}

	@Override
	protected FahrtImpl fillImpl(ResultSet resultSet) {
		FahrtImpl impl = new FahrtImpl();
		try {
			impl.setSurrkey(resultSet.getInt("surrkey_fahrt"));
			impl.setA_nr(resultSet.getInt("a_nr"));
			impl.setAbsender(resultSet.getString("absender"));
			impl.setBemerkung(resultSet.getString("bemerkung"));
			impl.setDate(resultSet.getDate("date"));
			impl.setEmpfaenger(resultSet.getString("empfaenger"));
			impl.setKm(resultSet.getDouble("km"));
			impl.setPreis(resultSet.getDouble("preis"));
			impl.setFahrer_nr(resultSet.getInt("fahrer_nr"));
			impl.setPreis_fahrer(resultSet.getDouble("preis_fahrer"));
			impl.setStopp(resultSet.getInt("stopp"));
			impl.setSurrkey_preis(resultSet.getInt("surrkey_preis"));
			impl.setSurrkey_rechnung(resultSet.getInt("surrkey_rechnung"));
			impl.setSurrkey_tour(resultSet.getInt("surrkey_tour"));
			impl.setWzlz(resultSet.getDouble("WzLz"));
			impl.setKm_fahrer(resultSet.getDouble("km_fahrer"));
		}catch(SQLException e) {
			
		}		
		return impl;
	}

	
	public boolean saveFahrtImpl(FahrtImpl fahrtImpl) throws DatenbankException {
		String sql = createInsertOrUpdateStatment(fahrtImpl);
		boolean saved = excecute(sql);
		return saved;
	}
	
	
	public List<FahrtImpl> findAll(String... wheres) {
		List<FahrtImpl> implList = new  ArrayList<FahrtImpl>();
		String sql = "select * from r_fahrt";
		String where = generateWhereStatment(wheres);
		sql += where.equals("")?"": " where " + where;
		sql+= " order by date";
		ResultSet resultSet = find(sql);
		if(resultSet != null) {
			try {
				while(resultSet.next()){
					implList.add(fillImpl(resultSet));
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return implList;
	}
	
	
	public List<FahrtImpl> findAllForKunde(int surrkey_kunde, Date von, Date bis) {
  		String sql = "select * from r_fahrt where surrkey_rechnung in (select surrkey_rechnung from r_rechnung where surrkey_kunde = " + surrkey_kunde + " ) ";
		
  		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		
		sql +=(von != null && bis != null)?" and date(date) between '" + format.format(von)+"' and '" + format.format(bis)+"' ":"";

		ResultSet resultSet = find(sql);
		List<FahrtImpl> implList = new ArrayList<FahrtImpl>();
		if(resultSet != null) {
			try {
				while(resultSet.next()) {
					implList.add(fillImpl(resultSet));
				}
			}catch(SQLException e) {
				e.printStackTrace();
			}
		}
		return implList;
		
	}
	
	
	
	public boolean haengeFahrtUm(int newSurrkey, FahrtImpl impl) {
		String sql = "update r_fahrt set surrkey_rechnung = " + newSurrkey + " where surrkey_fahrt = "+impl.getSurrkey();
		boolean saved = excecute(sql);
		return saved;
	}
}
