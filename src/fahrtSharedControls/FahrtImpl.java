package fahrtSharedControls;

import java.lang.reflect.Field;
import java.util.Date;
import java.util.HashMap;

import preiseSharedControls.PreisImpl;
import preiseSharedControls.PreisService;

import javax.print.attribute.standard.PresentationDirection;

import datenbankService.DatenbankException;
import sharedControls.AbstractImpl;
import sharedControls.UnvalideDataException;

public class FahrtImpl extends AbstractImpl<FahrtImpl> {

	


	private Date date = new Date();
	private int a_nr;
	private String absender ="";
	private String empfaenger ="";
	private double km;
	private double km_fahrer;
	
	
	
	private int stopp;
	private double wzlz;
	private double preis;
	private double preis_fahrer;
	private String bemerkung ="";
	private int fahrer_nr;

	
	private int surrkey_rechnung;
	private int surrkey_tour;
	private int surrkey_preis;
	
	
	public double getKm_fahrer() {
		return km_fahrer;
	}

	public void setKm_fahrer(double km_fahrer) {
		this.km_fahrer = km_fahrer;
	}

	
	
	
	
	public FahrtImpl() {
		super("r_fahrt", "surrkey_fahrt");
	}

	@Override
	public HashMap<String, Object> generateDatabaseHashSet() {
		HashMap< String, Object> data = new HashMap<String, Object>();
		data.put("surrkey_fahrt", getSurrkey());
		for(Field field:getClass().getDeclaredFields()) {
			try {
				data.put(field.getName(), field.get(this));
			} catch (IllegalArgumentException | IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		return data;
	}


	public String getBemerkung() {
		return bemerkung;
	}

	public void setBemerkung(String bemerkung) {
		this.bemerkung = bemerkung;
	}
	
	
	@Override
	public boolean isEqual(FahrtImpl abstractImpl) {
		return true;
	}

	@Override
	public boolean ueberpruefeDaten() throws UnvalideDataException {
		return true;
	}

	@Override
	public Object[] createSuchergebnis() {
		String fahrzeug ="";
		try {
			PreisService service = new PreisService();
			PreisImpl impl = service.findOne(surrkey_preis);
			fahrzeug = impl.getFahrzeug();
		} catch (DatenbankException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String formatAbsender = bemerkung.equals("")? absender: absender + "/" + bemerkung;
		
		return new Object[]{date, a_nr, formatAbsender, empfaenger, fahrzeug, fahrer_nr, km, stopp, wzlz, preis };
	}


	
	
	public int getFahrer_nr() {
		return fahrer_nr;
	}

	public void setFahrer_nr(int fahrer_nr) {
		this.fahrer_nr = fahrer_nr;
	}

	public double getPreis_fahrer() {
		return preis_fahrer;
	}

	public void setPreis_fahrer(double preis_fahrer) {
		this.preis_fahrer = preis_fahrer;
	}

	public int getSurrkey_preis() {
		return surrkey_preis;
	}

	public void setSurrkey_preis(int surrkey_preis) {
		this.surrkey_preis = surrkey_preis;
	}
	
	
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getA_nr() {
		return a_nr;
	}

	public void setA_nr(int a_nr) {
		this.a_nr = a_nr;
	}

	public String getAbsender() {
		return absender;
	}

	public void setAbsender(String absender) {
		this.absender = absender;
	}

	public String getEmpfaenger() {
		return empfaenger;
	}

	

	
	
	public void setEmpfaenger(String empfaenger) {
		this.empfaenger = empfaenger;
	}

	public double getKm() {
		return km;
	}

	public void setKm(double km) {
		this.km = km;
	}

	public int getStopp() {
		return stopp;
	}

	public void setStopp(int stopp) {
		this.stopp = stopp;
	}

	public double getWzlz() {
		return wzlz;
	}

	public void setWzlz(double wzlz) {
		this.wzlz = wzlz;
	}

	public double getPreis() {
		return preis;
	}

	public void setPreis(double preis) {
		this.preis = preis;
	}

	public int getSurrkey_rechnung() {
		return surrkey_rechnung;
	}

	public void setSurrkey_rechnung(int surrkey_rechnung) {
		this.surrkey_rechnung = surrkey_rechnung;
	}

	public int getSurrkey_tour() {
		return surrkey_tour;
	}

	public void setSurrkey_tour(int surrkey_tour) {
		this.surrkey_tour = surrkey_tour;
	}

}
