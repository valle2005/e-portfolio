package kundeSharedControls;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import datenbankService.DatenbankException;
import datenbankService.DatenbankService;

public class KundeService extends DatenbankService<KundeImpl>{

	public KundeService() throws DatenbankException  {
		super();	
	}

	@Override
	protected KundeImpl fillImpl(ResultSet resultSet) {
		KundeImpl kundeImpl = new KundeImpl();
			try {
				if(resultSet.getInt("surrkey_kunde") != 0)
					kundeImpl.setSurrkey(resultSet.getInt("surrkey_kunde"));
				if(resultSet.getInt("kunden_nr") != 0)
					kundeImpl.setKundenNr(resultSet.getInt("kunden_nr"));
				if(resultSet.getString("name") != null) 
					kundeImpl.setName(resultSet.getString("name"));
				if(resultSet.getString("strasse") != null) 
					kundeImpl.setStrasse(resultSet.getString("strasse"));
				if(resultSet.getString("hausnummer") != null) 
					kundeImpl.setHausnummer(resultSet.getString("hausnummer"));
				if(resultSet.getString("plz") != null) 
					kundeImpl.setPlz(resultSet.getString("plz"));
				if(resultSet.getString("ort") != null) 
					kundeImpl.setOrt(resultSet.getString("ort"));
				if(resultSet.getString("tel_nr") != null) 
					kundeImpl.setTelNr(resultSet.getString("tel_nr"));
				if(resultSet.getString("fax_nr") != null) 
					kundeImpl.setFaxNr(resultSet.getString("fax_nr"));
				if(resultSet.getString("e_mail") != null) 
					kundeImpl.seteMail(resultSet.getString("e_mail"));
				if(resultSet.getString("anmerkung") != null) 
					kundeImpl.setAnmerkung(resultSet.getString("anmerkung"));
				if(resultSet.getInt("preis_id") != 0)
					kundeImpl.setPreisId(resultSet.getInt("preis_id"));
				if(resultSet.getDouble("zeitintervall") != 0)
					kundeImpl.setZeitintervall(resultSet.getDouble("zeitintervall"));
				kundeImpl.setMonatlich(resultSet.getBoolean("monatlich"));
				return kundeImpl;
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
				
			}
		
		
	}

	public int getNextSurrkey() throws DatenbankException {
		try {
			return generateNewSurrkey("s_kunde", "surrkey_kunde");
		} catch (SQLException e) {
			throw new DatenbankException(e);
		}
	}
	
	
	public boolean saveKundeImpl (KundeImpl kundeImpl) {
		String sql = createInsertOrUpdateStatment(kundeImpl);
		boolean saved = excecute(sql);
		return saved;
	}
	
	public  KundeImpl findOne(int surrkey) throws DatenbankException  {
		String sql = "select * from s_kunde where surrkey_kunde= " + surrkey;
		ResultSet resultSet = find(sql);
		KundeImpl kundeImpl = new KundeImpl();
		try {
			while(resultSet.next()) {
				kundeImpl = fillImpl(resultSet);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DatenbankException(e);
		}
		return kundeImpl;
	}
	
	public List<KundeImpl> findAll(String... wheres) throws DatenbankException {
		List<KundeImpl> listeKundeImpl = new ArrayList<KundeImpl>();
		String sql = "select * from s_kunde";
		String whereStatment = generateWhereStatment(wheres);
		sql += !whereStatment.equals("")? " where " + whereStatment:"";
		ResultSet resultSet = find(sql);
		try {
			if(resultSet != null) {
				while(resultSet.next()) {
					listeKundeImpl.add(fillImpl(resultSet));
				}
			}
		} catch (SQLException e) {
			throw new DatenbankException(e);
		}
		return listeKundeImpl;
		
	}
	
	public int getNextKundenNr() throws DatenbankException {
		int kundennummer;
		try {
			kundennummer = generateNewSurrkey("s_kunde", "kunden_nr");
		} catch (SQLException e) {
			throw new DatenbankException(e);
		}
		return kundennummer;
	}
	
}
