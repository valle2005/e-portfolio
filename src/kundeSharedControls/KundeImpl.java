 package kundeSharedControls;

import java.lang.reflect.Field;
import java.util.HashMap;

import javax.swing.table.DefaultTableModel;

import sharedControls.AbstractImpl;
import sharedControls.UnvalideDataException;

public class KundeImpl extends AbstractImpl<KundeImpl>{

	
	private int kunden_nr = 0;
	private String name = "";
	private String strasse ="";
	private String hausnummer = "";
	private String plz = "";
	private String ort ="";
	private double zeitintervall =0;
	private String anmerkung ="";
	private boolean monatlich = false;


	private String tel_nr ="";
	private String fax_nr ="";
	private String e_mail ="";
	private int preis_id =0;
	
	
	public KundeImpl() {
		super("S_KUNDE", "surrkey_kunde");
	}
	
	@Override 
	public String toString() {
		return name;
	}
	
	@Override
	public Object[] createSuchergebnis() {
		return new Object []{this.getKundenNr(), this.getName(), this.getZeitintervall() + (monatlich?" Monaten": " Tagen"), this.getOrt()};
	}
	
	@Override
	public boolean ueberpruefeDaten() throws UnvalideDataException {
		if(kunden_nr < 1000) throw new UnvalideDataException("Kundennummer", "Muss gr��er sein als 1000");
		if(anmerkung.length() > 1000) throw new UnvalideDataException("Anmerkung", "Darf nicht l�nger sein als 1000 Zeilen");
		if(!e_mail.contains("@") && !e_mail.equals("")) throw new UnvalideDataException("E-Mail", "Fehlerhafte E-Mail");
		if(name.equals("")) throw new UnvalideDataException("Name", "Name darf nicht leer sein");
		return true;
	}
	
	
	
	
	@Override
	public boolean isEqual(KundeImpl actualKundeImpl) {
		if(kunden_nr != actualKundeImpl.getKundenNr())return false;
		if(!anmerkung.equals(actualKundeImpl.getAnmerkung())) return false;
		if(!e_mail.equals(actualKundeImpl.geteMail())) return false;
		if(!fax_nr.equals(actualKundeImpl.getFaxNr())) return false;
		if(!hausnummer.equals(actualKundeImpl.getHausnummer())) return false;
		if(!name.equals(actualKundeImpl.getName())) return false;
		if(!ort.equals(actualKundeImpl.getOrt())) return false;
		if(!plz.equals(actualKundeImpl.getPlz())) return false;
		if(!strasse.equals(actualKundeImpl.getStrasse())) return false;
		if(zeitintervall != actualKundeImpl.getZeitintervall()) return false;
		if(!tel_nr.equals(actualKundeImpl.getTelNr())) return false;
		if(preis_id != actualKundeImpl.getPreisId()) return false;
		if(monatlich != actualKundeImpl.isMonatlich()) return false;
		return true;
	}

	
	@Override
	public HashMap<String, Object> generateDatabaseHashSet() {
		HashMap<String, Object> dataMap = new HashMap<String, Object>();
		dataMap.put("surrkey_kunde", getSurrkey());
		for(Field field:getClass().getDeclaredFields()) {
			try {
				dataMap.put(field.getName(), field.get(this));
			} catch (IllegalArgumentException | IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return dataMap;
	}
	
	
	
	public String getAnmerkung() {
		return anmerkung;
	}

	public void setAnmerkung(String anmerkung) {
		this.anmerkung = anmerkung;
	}

	public int getKundenNr() {
		return kunden_nr;
	}

	public void setKundenNr(int kundenNr) {
		this.kunden_nr = kundenNr;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStrasse() {
		return strasse;
	}

	public void setStrasse(String strasse) {
		this.strasse = strasse;
	}

	public String getHausnummer() {
		return hausnummer;
	}

	public void setHausnummer(String hausnummer) {
		this.hausnummer = hausnummer;
	}

	public String getPlz() {
		return plz;
	}

	public void setPlz(String plz) {
		this.plz = plz;
	}

	public String getOrt() {
		return ort;
	}

	public void setOrt(String ort) {
		this.ort = ort;
	}

	public double getZeitintervall() {
		return zeitintervall;
	}

	public void setZeitintervall(double zeitintervall) {
		this.zeitintervall = zeitintervall;
	}

	public String getTelNr() {
		return tel_nr;
	}

	public void setTelNr(String telNr) {
		this.tel_nr = telNr;
	}

	public String getFaxNr() {
		return fax_nr;
	}

	public void setFaxNr(String faxNr) {
		this.fax_nr = faxNr;
	}

	public String geteMail() {
		return e_mail;
	}

	public void seteMail(String eMail) {
		this.e_mail = eMail;
	}

	public int getPreisId() {
		return preis_id;
	}

	public void setPreisId(int preisId) {
		this.preis_id = preisId;
	}

	public int getKunden_nr() {
		return kunden_nr;
	}

	public void setKunden_nr(int kunden_nr) {
		this.kunden_nr = kunden_nr;
	}

	public boolean isMonatlich() {
		return monatlich;
	}

	public void setMonatlich(boolean monatlich) {
		this.monatlich = monatlich;
	}

	public String getTel_nr() {
		return tel_nr;
	}

	public void setTel_nr(String tel_nr) {
		this.tel_nr = tel_nr;
	}

	public String getFax_nr() {
		return fax_nr;
	}

	public void setFax_nr(String fax_nr) {
		this.fax_nr = fax_nr;
	}

	public String getE_mail() {
		return e_mail;
	}

	public void setE_mail(String e_mail) {
		this.e_mail = e_mail;
	}

	public int getPreis_id() {
		return preis_id;
	}

	public void setPreis_id(int preis_id) {
		this.preis_id = preis_id;
	}



	



	


	
}
