package empfaengerSharedControls;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import datenbankService.DatenbankException;
import datenbankService.DatenbankService;

public class EmpfaengerService extends DatenbankService<EmpfaengerImpl>{

	public EmpfaengerService() throws DatenbankException {
		super();
	}

	@Override
	protected EmpfaengerImpl fillImpl(ResultSet resultSet) {
		EmpfaengerImpl impl = new EmpfaengerImpl();
		try {
			impl.setEmpfaenger(resultSet.getString("empfaenger"));
			impl.setEmpfaenger_id(resultSet.getInt("empfaenger_id"));
			impl.setHausnummer(resultSet.getString("hausnummer"));
			impl.setKm(resultSet.getDouble("km"));
			impl.setOrt(resultSet.getString("ort"));
			impl.setPlz(resultSet.getString("plz"));
			impl.setStrasse(resultSet.getString("strasse"));	
			impl.setSurrkey(resultSet.getInt("surrkey_empfaenger"));
			impl.setSurrkey_tour(resultSet.getInt("surrkey_tour"));
			impl.setTelefon(resultSet.getString("telefon"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return impl;
	}
	
	
	public List<EmpfaengerImpl> findAll(String... wheres) throws DatenbankException {
		String sql = "select * from s_empfaenger";
		String where = generateWhereStatment(wheres);
		sql += !where.equals("")?" where " + where:"";
		sql += " order by empfaenger_id";
		List<EmpfaengerImpl> liste = new ArrayList<EmpfaengerImpl>();
		ResultSet resultSet = find(sql);
		try {
			if(resultSet != null) {
				while (resultSet.next()) {
					liste.add(fillImpl(resultSet));
				}
			}
			
		} catch (SQLException e) {
			throw new DatenbankException(e);
		}
		
		return liste;
	}
	
	public boolean saveOne(EmpfaengerImpl impl) {
		String sql = createInsertOrUpdateStatment(impl);
		boolean saved = excecute(sql);
		return saved;
	}
	
	

}
