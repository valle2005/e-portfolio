package empfaengerSharedControls;

import java.lang.reflect.Field;
import java.util.HashMap;

import sharedControls.AbstractImpl;
import sharedControls.UnvalideDataException;

public class EmpfaengerImpl extends AbstractImpl<EmpfaengerImpl>{

	private String empfaenger ="";
	private int empfaenger_id;
	private String hausnummer ="";
	private double km;
	private String ort = "";
	private String plz = "";
	private String strasse ="";
	private int surrkey_tour;
	private String telefon ="";
	
	
	public EmpfaengerImpl() {
		super("s_empfaenger", "surrkey_empfaenger");
	}

	@Override
	public HashMap<String, Object> generateDatabaseHashSet() {
		HashMap< String, Object> data = new HashMap<String, Object>();
		data.put("surrkey_empfaenger", getSurrkey());
		for(Field field:getClass().getDeclaredFields()) {
			try {
				data.put(field.getName(), field.get(this));
			} catch (IllegalArgumentException | IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		return data;
	}

	@Override
	public boolean isEqual(EmpfaengerImpl abstractImpl) {
		if(!empfaenger.equals(abstractImpl.getEmpfaenger())) return false;
		if(empfaenger_id != abstractImpl.getEmpfaenger_id()) return false;
		if((!hausnummer.equals(abstractImpl.getHausnummer()))) return false;
		if(km != abstractImpl.getKm()) return false;
		if(!ort.equals(abstractImpl.getOrt())) return false;
		if(!plz.equals(abstractImpl.getPlz())) return false;
		if(!strasse.equals(abstractImpl.getStrasse())) return false;
		if(surrkey_tour != abstractImpl.getSurrkey_tour()) return false;
		if(!telefon.equals(abstractImpl.getTelefon())) return false;
		return true;
	}

	@Override
	public boolean ueberpruefeDaten() throws UnvalideDataException {
		if(empfaenger.equals("")) throw new UnvalideDataException("Empf�nger", "Empf�nger darf nicht leer sein");
		if(ort.equals("")) throw new UnvalideDataException("Ort", "Ort darf nicht leer sein");
		return true;
	}
	
	public String toString() {
		return empfaenger + " (" + ort + ")";
	}

	@Override
	public Object[] createSuchergebnis() {
		return new Object[] {empfaenger, ort, km, this};
	}

	public synchronized String getEmpfaenger() {
		return empfaenger;
	}

	public synchronized void setEmpfaenger(String empfaenger) {
		this.empfaenger = empfaenger;
	}

	public synchronized int getEmpfaenger_id() {
		return empfaenger_id;
	}

	public synchronized void setEmpfaenger_id(int empfaenger_id) {
		this.empfaenger_id = empfaenger_id;
	}

	public synchronized String getHausnummer() {
		return hausnummer;
	}

	public synchronized void setHausnummer(String hausnummer) {
		this.hausnummer = hausnummer;
	}

	public synchronized double getKm() {
		return km;
	}

	public synchronized void setKm(double km) {
		this.km = km;
	}

	public synchronized String getOrt() {
		return ort;
	}

	public synchronized void setOrt(String ort) {
		this.ort = ort;
	}

	public synchronized String getPlz() {
		return plz;
	}

	public synchronized void setPlz(String plz) {
		this.plz = plz;
	}

	public synchronized String getStrasse() {
		return strasse;
	}

	public synchronized void setStrasse(String strasse) {
		this.strasse = strasse;
	}

	public synchronized int getSurrkey_tour() {
		return surrkey_tour;
	}

	public synchronized void setSurrkey_tour(int surrkey_tour) {
		this.surrkey_tour = surrkey_tour;
	}

	public synchronized String getTelefon() {
		return telefon;
	}

	public synchronized void setTelefon(String telefon) {
		this.telefon = telefon;
	}

	
	
}
