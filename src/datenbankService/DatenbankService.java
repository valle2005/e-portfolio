package datenbankService;

import java.util.List;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;

import RechnungSharedControls.RechnungImpl;
import sharedControls.AbstractImpl;

public abstract class DatenbankService<T extends AbstractImpl<?>> {
	
	private Connection connection = null;
	private final  String CONNECTION_STRING ="jdbc:mysql://localhost/city_kurier_datenbank_test";
	private final String USER = "root";
	//private final String PASSWORD = "your_password";
	
	
	public DatenbankService () throws DatenbankException {
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		try {
			connection = DriverManager.getConnection(CONNECTION_STRING, USER,"");
		}catch(SQLException ex) {
			throw new DatenbankException(ex);
		}
		
	}
	
	
	
	protected abstract T fillImpl(ResultSet resultSet);
	
	
	
	protected  String  createInsertOrUpdateStatment(T abstractImpl)  {
		try {
			
			if(abstractImpl.getSurrkey() == -1) {
				int neuerSurrkey = generateNewSurrkey(abstractImpl.getTableName(), abstractImpl.getSurrkeyField());
				abstractImpl.setSurrkey(neuerSurrkey);
				if(abstractImpl instanceof RechnungImpl) {
					RechnungImpl rechImpl = (RechnungImpl) abstractImpl;
					rechImpl.setRech_nr(generateNewSurrkey(rechImpl.getTableName(), "rech_nr"));
				}
				HashMap<String, Object > dataset = abstractImpl.generateDatabaseHashSet();
				return generateInsertStatement(dataset, abstractImpl.getTableName());
			}else {
				HashMap<String, Object> dataset = abstractImpl.generateDatabaseHashSet();
				return generateUpdateStatement(dataset, abstractImpl.getTableName(), abstractImpl.getSurrkeyField());
			}
		}catch(SQLException ex) {
				return "";
		}
	}
	
	
	public boolean deleteImp(AbstractImpl abstractImpl) {
		String sql = "Delete from " + abstractImpl.getTableName() + 
					" where " + abstractImpl.getSurrkeyField() + "=" + abstractImpl.getSurrkey();
		return excecute(sql);	
	}
	
	
	private String generateInsertStatement (HashMap<String, Object> dataset, String tableName) {
		String sql = "INSERT INTO " + tableName +" (";
		String columns = "";
		String values = ""; 
		for(String key: dataset.keySet()) {		
			columns += ", " + key ;
			Object ob = dataset.get(key);
			if(ob.getClass().equals(String.class)) {
				values += ", \"" + (String) ob + "\"";
			}else {
				if(ob.getClass().equals(Date.class)) {
					values += ", '" + new java.sql.Date(((Date) ob).getTime()) + "'";
				}else {
					values += ", " +  ob.toString();
				}
				
			}
			
		}
		columns = columns.substring(1);
		values = values.substring(1);
		sql +=columns + ") values(" + values + ")";
		return sql;
	}
	
	private String generateUpdateStatement(HashMap<String, Object> dataset, String tablename, String surrkeyfield) {
		String sql = "update " + tablename + " set ";
		String values = "";
		for(String key: dataset.keySet()) {
			Object ob = dataset.get(key);
			if(ob.getClass().equals(String.class)) {
				values += ", " + key + "=\"" + (String) ob + "\"";
			}else {
				if(ob instanceof Date) {
					values += ",   "+ key+"='" + new java.sql.Date(((Date) ob).getTime()) + "'";
				}else {
					values += ", " + key +"="+ ob.toString();
				}
			}
		}
		values = values.substring(1);
		sql += values + " where " + surrkeyfield + "=" +dataset.get(surrkeyfield);
		return sql;
	}
	
	protected int generateNewSurrkey(String tableName, String surrkey_field) throws SQLException {
		String sql = "select max(" +surrkey_field+") from " + tableName;
		ResultSet resultSet = find(sql);
		int neuerSurrkey = 0;
				
		while (resultSet.next()) {
			neuerSurrkey = resultSet.getInt("max("+surrkey_field+")");
		}
		neuerSurrkey = neuerSurrkey +1;
		return neuerSurrkey;
	}
	
	
	protected String generateWhereStatment(String... wheres) {
		String whereStatment ="";
		for(int i = 0; i < wheres.length; i++) {
			whereStatment += wheres[i];
			if((i+1) <wheres.length && !wheres[i+1].equals("") && !whereStatment.equals("")) {
				whereStatment += " and ";
			}
			
		}
		return whereStatment;
	}
	
	
	
	protected boolean excecute(String sql) {
		try {
			Statement sqlStatement = connection.createStatement();
			sqlStatement.execute(sql);
			return true;  
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	protected ResultSet find(String sql) {
		try {
			Statement sqlStatement = connection.createStatement();
			return sqlStatement.executeQuery(sql);
			
		}catch(SQLException e) {
			return null;
		}
		
		
		
		
		
	}
	
	

}
