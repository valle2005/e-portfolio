package sharedControls;

public class UnvalideDataException extends Exception{
	
	private String dataname ="";
	private String message ="";
	
	public UnvalideDataException (String dataname, String message){
		this.dataname = dataname;
		this.message =  message;
	}
	
	public String toString() {
		return dataname +" falsch: " + message;
	}

	
	
}
