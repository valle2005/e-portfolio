package sharedControls;

import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.table.DefaultTableModel;

public abstract class AbstractImpl<T extends Object> {

	
	
	protected int surrkey = -1;
	protected String tableName ="";
	protected String surrkey_field = "";
	
	public AbstractImpl (String tableName, String surrkey_field) {
		this.surrkey_field = surrkey_field;
		this.tableName = tableName;
	}
	
	public String getSurrkeyField() {
		return this.surrkey_field;
	}
	
	public void setSurrkey(int surrkey) {
		this.surrkey = surrkey;
	}
	
	public int getSurrkey() {
		return surrkey;
	}
	
	public String getTableName() {
		return this.tableName;
	}
	
	
	public abstract HashMap<String, Object> generateDatabaseHashSet();
	public abstract boolean isEqual(T abstractImpl);
	public abstract boolean ueberpruefeDaten() throws UnvalideDataException;
	public abstract Object[] createSuchergebnis();
	
}
