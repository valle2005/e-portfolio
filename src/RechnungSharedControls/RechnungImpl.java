package RechnungSharedControls;

import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import datenbankService.DatenbankException;
import kundeSharedControls.KundeImpl;
import kundeSharedControls.KundeService;
import sharedControls.AbstractImpl;
import sharedControls.UnvalideDataException;

public class RechnungImpl extends AbstractImpl<RechnungImpl>{

	
	private int kunden_nr;
	private int rech_nr;
	private Date rech_datum;
	private String status = "";
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	private int surrkey_Kunde;
	private int surrkey_rechnungsempfaenger;
	
	
	public RechnungImpl() {
		super("r_rechnung", "surrkey_rechnung");
	}

	@Override
	public HashMap<String, Object> generateDatabaseHashSet() {
		HashMap< String, Object> data = new HashMap<String, Object>();
		data.put("surrkey_rechnung", getSurrkey());
		for(Field field:getClass().getDeclaredFields()) {
			try {
				data.put(field.getName(), field.get(this));
			} catch (IllegalArgumentException | IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		return data;

	}

	@Override
	public boolean isEqual(RechnungImpl abstractImpl) {
		return false;
	}

	@Override
	public boolean ueberpruefeDaten() throws UnvalideDataException {
		return false;
	}
	
	public String toString() {
		DateFormat format = new SimpleDateFormat("dd.MM.yyy");
		KundeService kundeService;
		try {
			kundeService = new KundeService();
			KundeImpl kundeImpl = kundeService.findOne(this.getSurrkey_Kunde());
			Calendar c = Calendar.getInstance();
			c.setTime(rech_datum);
			
			if(!kundeImpl.isMonatlich()) {
				c.add(Calendar.DATE,(int) kundeImpl.getZeitintervall());
				
			}else {
				int rechDate =  (int) (kundeImpl.getZeitintervall() * c.getActualMaximum(Calendar.DAY_OF_MONTH));
				int i = c.get(Calendar.DATE);
				if(rechDate > c.get(Calendar.DATE)) {
					c.set(Calendar.DATE, (int) (kundeImpl.getZeitintervall() * c.getActualMaximum(Calendar.DAY_OF_MONTH)));
				}else {
					c.set(Calendar.DATE, c.getActualMaximum(Calendar.DAY_OF_MONTH));
				}
				
			}
			return this.getRech_nr()+":   " + format.format(rech_datum) + "-" + format.format(c.getTime())+"|  "+ kundeImpl.getName();
		} catch (DatenbankException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return super.toString();
	}

	@Override
	public Object[] createSuchergebnis() {
		// TODO Auto-generated method stub
		return null;
	}

	public int getKunden_nr() {
		return kunden_nr;
	}

	public void setKunden_nr(int kunden_nr) {
		this.kunden_nr = kunden_nr;
	}

	public int getRech_nr() {
		return rech_nr;
	}

	public void setRech_nr(int rech_nr) {
		this.rech_nr = rech_nr;
	}

	public Date getRech_datum() {
		return rech_datum;
	}

	public void setRech_datum(Date rech_datum) {
		this.rech_datum = rech_datum;
	}


	public int getSurrkey_Kunde() {
		return surrkey_Kunde;
	}

	public void setSurrkey_Kunde(int surrkey_Kunde) {
		this.surrkey_Kunde = surrkey_Kunde;
	}
	
	public int getSurrkey_rechnungsempfaenger() {
		return surrkey_rechnungsempfaenger;
	}

	public void setSurrkey_rechnungsempfaenger(int surrkey_rechnungsempfaenger) {
		this.surrkey_rechnungsempfaenger = surrkey_rechnungsempfaenger;
	}


}
