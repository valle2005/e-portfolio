package RechnungSharedControls;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.swing.CellEditor;

import kundeSharedControls.KundeImpl;
import datenbankService.DatenbankException;
import datenbankService.DatenbankService;

public class RechnungService extends DatenbankService<RechnungImpl>{

	public RechnungService() throws DatenbankException {
		super();
	}

	@Override
	protected RechnungImpl fillImpl(ResultSet resultSet) {
		RechnungImpl impl = new RechnungImpl();
		try {
			impl.setKunden_nr(resultSet.getInt("kunden_nr"));
			impl.setRech_datum(resultSet.getDate("rech_datum"));
			impl.setRech_nr(resultSet.getInt("rech_nr"));
			impl.setSurrkey(resultSet.getInt("surrkey_rechnung"));
			impl.setSurrkey_Kunde(resultSet.getInt("surrkey_kunde"));
			impl.setSurrkey_rechnungsempfaenger(resultSet.getInt("surrkey_rechnungsempfaenger"));
			impl.setStatus(resultSet.getString("status"));
		}catch(SQLException e) {
			
		}
		
		return impl;
	}

	
	public boolean saveOne(RechnungImpl abstractImpl) {
		String sql = createInsertOrUpdateStatment(abstractImpl);
		boolean saved = excecute(sql);
		return saved;
		
	}
	
	public int getNextSurrkey() throws SQLException {
		return generateNewSurrkey("r_rechnung", "surrkey_rechnung");
	}
	
	
	public int getNextRechnungsnummer() throws DatenbankException {
		String sql = "select max(rech_nr) from r_rechnung";
		ResultSet resultSet = find(sql);
		int rechnr = 0;
		try {
			while(resultSet.next()) {
				rechnr = resultSet.getInt("max(rech_nr)");
			}
		} catch (SQLException e) {
			throw new DatenbankException(e);
		}
		rechnr ++;
		return rechnr;
	}
	
	public RechnungImpl findRechnung(Date datum, int surrkey_empfaenger) throws DatenbankException {
		RechnungImpl rechnungImpl = null;
		String whereDatum = "rech_datum = '" + new java.sql.Date(datum.getTime()) +"'";
		String whereSurr = "surrkey_rechnungsempfaenger = "+ surrkey_empfaenger;
		String where = generateWhereStatment(whereDatum, whereSurr);
		String sql = "select * from r_rechnung";
		sql += where.equals("")? "": " where " + where;
		sql += " order by rech_nr ";
		ResultSet resultSet = find(sql);
		if(resultSet != null) {
			try {
				while(resultSet.next()) {
					rechnungImpl = fillImpl(resultSet);
				}
			} catch (SQLException e) {
				throw new DatenbankException(e);
			}
		}
		return rechnungImpl;
	}
	
	
	public RechnungImpl findRechnungByNr (int rechNr) throws DatenbankException {
		RechnungImpl rechnungImpl = null;
		String sql = "select * from r_rechnung where rech_nr = "  + rechNr;
		ResultSet resultSet = find(sql);
		if(resultSet != null) {
			try {
				while(resultSet.next()) {
					rechnungImpl = fillImpl(resultSet);
				}
			} catch (SQLException e) {
				throw new DatenbankException(e);
			}
		}
		return rechnungImpl;
	}
	
	public List<RechnungImpl> findAll(String...wheres)  throws DatenbankException{
		List<RechnungImpl> implList = new ArrayList<RechnungImpl>();
		String where = generateWhereStatment(wheres);
		String sql = "select * from r_rechnung";
		sql += where.equals("")?"": " where " + where;
		sql += " order by rech_nr";
		ResultSet resultSet = find(sql);
		if(resultSet != null) {
			try {
				while(resultSet.next()) {
					implList.add(fillImpl(resultSet));
				}
			} catch (SQLException e) {
				throw new DatenbankException(e);
			}
		}
		return implList;
		
	}
	
	
	
}
