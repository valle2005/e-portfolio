package zusatzleistungSharedControls;

import java.lang.reflect.Field;
import java.util.HashMap;

import sharedControls.AbstractImpl;
import sharedControls.UnvalideDataException;

public class ZusatzleistungImpl extends AbstractImpl<ZusatzleistungImpl>{

	
	private double ladehelfer = 0.00;
	private double lh_ab_30 = 0;
	
	
	public ZusatzleistungImpl() {
		super("s_zusatzleistung", "surrkey_zusatzleistung");
	}

	@Override
	public HashMap<String, Object> generateDatabaseHashSet() {
		HashMap< String, Object> data = new HashMap<String, Object>();
		data.put("surrkey_zusatzleistung", getSurrkey());
		for(Field field:getClass().getDeclaredFields()) {
			try {
				data.put(field.getName(), field.get(this));
			} catch (IllegalArgumentException | IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return data;
	}
	@Override
	public boolean isEqual(ZusatzleistungImpl abstractImpl) {
		if(ladehelfer != abstractImpl.getLadehelfer()) return false;
		if(lh_ab_30 != abstractImpl.getLh_ab_30()) return false;
		return true;
	}
	
	@Override
	public boolean ueberpruefeDaten() throws UnvalideDataException {
		return false;
	}

	@Override
	public Object[] createSuchergebnis() {
		return null;
	}
	

	public synchronized double getLadehelfer() {
		return ladehelfer;
	}

	public synchronized void setLadehelfer(double ladehelfer) {
		this.ladehelfer = ladehelfer;
	}

	public synchronized double getLh_ab_30() {
		return lh_ab_30;
	}

	public synchronized void setLh_ab_30(double lh_ab_20) {
		this.lh_ab_30 = lh_ab_20;
	}

	

}
