package zusatzleistungSharedControls;

import java.sql.ResultSet;
import java.sql.SQLException;

import datenbankService.DatenbankException;
import datenbankService.DatenbankService;

public class ZusatzleistungService extends DatenbankService<ZusatzleistungImpl>{

	public ZusatzleistungService() throws DatenbankException {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	protected ZusatzleistungImpl fillImpl(ResultSet resultSet) {
		ZusatzleistungImpl impl = new ZusatzleistungImpl();
		try {
			impl.setSurrkey(resultSet.getInt("surrkey_zusatzleistung"));
			impl.setLadehelfer(resultSet.getDouble("ladehelfer"));
			impl.setLh_ab_30(resultSet.getDouble("lh_ab_30"));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return impl;
	}

	
	public int getNextSurrkey()  throws DatenbankException{
		try {
			return generateNewSurrkey("s_zusatzleistung", "surrkey_zusatzleistung");
		} catch (SQLException e) {
			throw new DatenbankException(e);
		}
	}
	
	public boolean saveZusatzleistungImpl(ZusatzleistungImpl impl)  {
		String sql = createInsertOrUpdateStatment(impl);
		boolean saved = excecute(sql);
		System.out.println();
		return saved;
	}
	
	public ZusatzleistungImpl findOne(int surrkey) throws DatenbankException{
		String sql = "select * from s_zusatzleistung where surrkey_zusatzleistung= " + surrkey;
		ZusatzleistungImpl impl = new ZusatzleistungImpl();
		ResultSet resultSet = find(sql);
		if(resultSet != null) {	
			try {
				while(resultSet.next()) {
					impl = fillImpl(resultSet);
				}
			} catch (SQLException e) {
				throw new DatenbankException(e);
			}
		}
		return impl;
	}
	
}
